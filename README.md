# environments
for promoting code across operating environments

## usage
+-------------------------------------+-------------------------------------+
| evx --help [command]                | list available (sub-)commands       |
| evx --version                       | display the evx version             |
| evx status                          | display status                      |
| evx list                            | list all available environments     |
| evx major [version]                 | increment current major version     |
| evx minor [version]                 | increment current minor version     |
| evx patch [version]                 | increment current patch veresion    |      
| evx promote <version>               | promote version to next environment |
+-------------------------------------+-------------------------------------+ 

## purpose
environments (or evx for short) offers a programmable, configurable framework
for deploying software to multiple operating environments. if it can be scripted
with nodejs, it can be scripted within the evx framework. you define the operating
environments, the hierarchical promotion and integrate the actions take when
your software gets promoted to each environment.

# worry-free deployments
if any deployment fails, you can define behaviors for cleaning up so that you
are able to re-attempt from a known, predictable state.
