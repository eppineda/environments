'use strict';

const { Command } = require('commander')
const { readFile } = require('fs/promises')
const content = {
  'help': './help/help.txt',
}
const set = function(c) {
  this.configuration = c
  console.log(`${ this.configuration }`.gray)
  return this
}

const go = function() {
  let quit = false
  const program = new Command

  program.version('0.0.0')
  program.option('-h, --help [command]', 'yo', help)
  program.parse()
  return this
}
const shell = {
  configuration: {},
  set,
  go,
}
const help = async (topic = 'help', previous) => {
  console.log(`retrieving help topic for ${ topic }`)

  const read = filespec => {; console.log(`attempting to read from ${ filespec }`.gray)
    const NOT_FOUND = `help topic for ${ topic } not found`

    return new Promise((resolve, reject) => {
      resolve('test')
    })
    //return await readFile(filespec)
  }

  console.log(await read(content[topic])) 
}

module.exports = shell
