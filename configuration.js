'use strict';

const fs = require('fs')
const colors = require('colors')
const readConfiguration = async function(filespec, more = parsed => parsed) {
  return new Promise((resolve, reject) => {
    fs.readFile(filespec, async (err, data) => {
        const key = 'environment'
        let parsed = JSON.parse(data)

        if (err && !parsed) {
          reject(`could not read configuration: ${ err || parsed }`)
        }

        try {
          parsed = more(parsed)
        } catch(e) {
          reject(e)
        }

        resolve(parsed)
    })
  })
}

module.exports = {
  read: readConfiguration,
}
