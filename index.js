'use strict';

const prettyjson = require('prettyjson');

(async (filespecs) => {
  const environments = require('./configuration')
  const configuration = []

  filespecs.forEach(async filespec => {
    configuration.push(new Promise(async (resolve, reject) => {
      console.log(`reading from ${ filespec }`.gray)

      const doNothing = x => x
      const getEnvironments = async parsed => {
        const localStorage = require('node-persist')
        const key = 'environment'
        const modified = { ...parsed }

        await localStorage.init({
          dir: './.environment',
          stringify: JSON.stringify,
          parse: JSON.parse,
          encoding: 'utf8',
          logging: false,  // can also be custom logging function
          ttl: false, // ttl* [NEW], can be true for 24h default or a number in MILLISECONDS or a valid Javascript Date object
          expiredInterval: 2 * 60 * 1000, // every 2 minutes the process will clean-up the expired cache
          // in some cases, you (or some other service) might add non-valid storage files to your
          // storage dir, i.e. Google Drive, make this true if you'd like to ignore these files and not throw an error
          forgiveParseErrors: false
        })
        modified.environments = modified.promotion.map(stage => Object.keys(stage)[0]) // always array of 1

        try {
          const locallyStored = await localStorage.getItem(key); console.log(`local storage: ${ JSON.stringify(parsed) }`.gray)

          await localStorage.setItem(key, modified); console.log(`local storage: ${ JSON.stringify(modified) }`.gray) // place in local storage as a backup
        } catch(e) {
          console.log(`unable to write to local storage: ${ e }`.yellow)
        } 
        return modified
      }
      const config = await environments.read(filespec, -1 < filespec.search(/environments.json/i) ? getEnvironments : doNothing)

      console.log(`${ JSON.stringify(config) }`.gray)
      resolve(config)
    }))
  })
  return Promise.all(configuration)
})([ './environments.json', './package.json' ])
.then(configuration => {
  const ready = require('./shell')

  configuration.forEach(c => {
    console.log(`${ JSON.stringify(c) }\n`.gray)
  })
  ready.set(configuration).go()
})
.catch(err => {
  console.log(` ${ err }`.red)
})
